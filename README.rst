socketio-client-deepfence
=========================

Python implementation of the socket.io client.

Design & goals
--------------

This implementation is inspired by the JavaScript `socket.io-client`_
implementation.

It is directly using `engineio_client_deepfence`_ as underlying engine.io layer.

Protocol parser is copied in parts and at least largely inspired from the
package `python-socketio`_ written by `Miguel Grinberg`_.

This socket.io client is using `gevent`_ for now. This is not a strict design
choice but a simplification for this first implementaion. Other asynchronous
frameworks are welcome for future versions.

Example
-------

::

    from socketio_client_deepfence.manager import Manager

    import gevent
    from gevent import monkey

    monkey.patch_socket()
    monkey.patch_ssl()

    import logging

    logging.basicConfig(level=logging.DEBUG)

    io = Manager('https', '127.0.0.1', 443, auto_connect=True, ssl_verify=False, params={"a": "b"})
    socketio = io.socket('/websockets')


    @socketio.on('pong')
    def on_pong(*args, **kwargs):
        print("on_pong")
        print(args)
        print(kwargs)


    @socketio.on('message')
    def on_message(*args, **kwargs):
        print("on_message")
        print(args)
        print(kwargs)


    @socketio.on_connect()
    def on_connect():
        print("on_connect")
        socketio.emit("ping")


    socketio.connect()
    gevent.wait()



Links
-----

engine.io client: `engineio_client_deepfence`_

.. _socket.io-client: https://github.com/socketio/socket.io-client
.. _python-socketio: https://github.com/miguelgrinberg/python-socketio
.. _Miguel Grinberg: https://github.com/miguelgrinberg
.. _gevent: http://gevent.org/
.. _engineio_client_deepfence: https://github.com/deepfence/engineio-client-deepfence
