# -*- coding: utf-8 -*-
"""
socketio-client-deepfence
----------------------

Socket.IO client.
"""
from setuptools import setup

setup(
    name='socketio-client-deepfence',
    version='0.1',
    url='http://github.com/deepfence/socketio-client-deepfence/',
    license='MIT',
    author='Ramanan R',
    author_email='ramanan@deepfence.io',
    description='Socket.IO client',
    long_description=open('README.rst').read(),
    packages=['socketio_client_deepfence'],
    zip_safe=False,
    include_package_data=True,
    platforms='any',
    install_requires=[
        'engineio-client-deepfence>=0.1',
        'gevent>=1.0.2',
    ],
    classifiers=[
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 3',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ]
)
