from socketio_client_deepfence.manager import Manager

import gevent
from gevent import monkey

monkey.patch_socket()
monkey.patch_ssl()

import logging

logging.basicConfig(level=logging.DEBUG)

io = Manager('https', '127.0.0.1', 443, auto_connect=True, ssl_verify=False, params={"a": "b"})
socketio = io.socket('/websockets')


@socketio.on('pong')
def on_pong(*args, **kwargs):
    print("on_pong")
    print(args)
    print(kwargs)


@socketio.on('message')
def on_message(*args, **kwargs):
    print("on_message")
    print(args)
    print(kwargs)


@socketio.on_connect()
def on_connect():
    print("on_connect")
    socketio.emit("ping")


socketio.connect()
gevent.wait()
