import socketio
# pip install python-socketio
from gevent import pywsgi

import logging

logging.basicConfig(level=logging.DEBUG)

io = socketio.Server(binary=True)

namespace = "/websockets"


def callback(*args):
    print("Client responded '%s' to welcome" % args)


@io.on('connect', namespace=namespace)
def connect(sid, *args):
    io.emit('message', {'some': 'data'}, room=sid, callback=callback, namespace=namespace)


@io.on('ping', namespace=namespace)
def ping(sid, data):
    print("Received message with '%s'" % data)
    io.emit('pong', {}, room=sid, namespace=namespace)


app = socketio.Middleware(io)
pywsgi.WSGIServer(('', 8000), app).serve_forever()
# pywsgi.WSGIServer(('127.0.0.1', 443), app, keyfile='server.key', certfile='server.crt').serve_forever()
